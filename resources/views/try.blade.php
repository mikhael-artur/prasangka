<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Tanpa Prasangka</title>
    </head>
    <body>
        @include('header')
        <div class="flex-center position-ref full-height">
            <div class="trybox">
                <form>
                    <div class="form-group tryform">
                        <label for="title">Tentang apa ini?</label>
                        <input type="text" class="form-control" id="title" placeholder="Title">
                    </div>
                    <div class="form-group tryform">
                        <label for="content">Coba ceritakan lebih lanjut</label>
                        <textarea class="form-control" id="content" rows="5"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary mb-2">Coba Terbuka!</button>
                </form>
            </div>
        </div>
    </body>
</html>
