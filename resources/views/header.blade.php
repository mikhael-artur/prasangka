<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Tanpa Prasangka</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('css/style.css')}}" rel="stylesheet">
    </head>
    <body>
        <div class="header">
            <a href="/"><b><s> Prasangka </s></b></a> | <a href="/explore">Jelajahi</a> | <a href="/try">Mulai</a>
        </div>
        <!-- SCRIPT -->
        <script src="{{asset('js/jquery-3.2.1.slim.min.js')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <!-- END SCRIPT -->
    </body>
</html>
