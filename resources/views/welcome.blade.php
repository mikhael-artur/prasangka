<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Tanpa Prasangka</title>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    <b><s> Prasangka </s></b>
                </div>

                <div class="links">
                    <b>
                        Setiap orang mempunyai masalah mereka sendiri, <br>
                        yang tidak ingin diketahui orang lain. <br>
                        Sebagian dari mereka memilih untuk menutup diri. <br>
                        Sebagian yang lain berani untuk terbuka. <br>
                        Jelajahi cerita dari mereka yang berusaha terbuka. <br>
                        Atau mulai ceritamu sendiri. <br>
                    </b>
                    <br>
                    <a href="/explore">Jelajahi</a> | <a href="/try">Mulai</a>
                </div>
            </div>
        </div>
        <!-- SCRIPT -->
        {{ HTML::script('js/jquery-3.2.1.slim.min.js') }}
        {{ HTML::script('js/bootstrap.min.js') }}
        <!-- END SCRIPT -->

    </body>
</html>
